package ep2;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractButton;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSeparator;


@SuppressWarnings("serial")
public class Application extends JFrame implements ActionListener {
	static boolean jogando = false;	

	public Application() {
    	   	
		
    	
    	setSize(580,560);
    	
    	setTitle("Space Combat Game");
    	setResizable(false);
    	setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    	setLocationRelativeTo(null);

    	setContentPane(new JLabel(new ImageIcon("images/spacemenu2.gif")));
    	
    	Container lado1 =  getContentPane();
    	Container lado2 = new JPanel();

    	lado1.setLayout(new BorderLayout());
    	lado1.add(BorderLayout.EAST,lado2);
    	lado2.setLayout(new GridLayout(4,1));
    	
    	JButton play = new JButton("Play");
    	JButton ranking = new JButton("Ranking");
    	JButton sobre = new JButton("Sobre");
    	JButton sair = new JButton("Sair");
    	
    	lado2.add(play);
    	lado2.add(ranking);
    	lado2.add(sobre);
    	lado2.add(sair);

    	
    	play.addActionListener(this);
    	
    	
    	play.addActionListener(new ActionListener() {
    		public void actionPerformed(ActionEvent e){
    			jogando = true;
    			
    			try {
    				tocaShoot();
    	        } 
    			catch (Exception e1) {
    	        	e1.printStackTrace();
    	        }

    			JFrame janelajogo = new JFrame();
    			janelajogo.setSize(580,560);
    			janelajogo.setTitle("Space Combat Game");
    			janelajogo.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    			janelajogo.setLocationRelativeTo(null);
    			janelajogo.setResizable(false);
    			janelajogo.setVisible(true);
    			setVisible(false);
    			
    			janelajogo.add(new Map());	

    		}
    	});

    	
    	sobre.addActionListener(new ActionListener(){
        	
        	public void actionPerformed(ActionEvent e){
        		JOptionPane.showMessageDialog(null, "Projeto desenvolvido por Miguel Alves para a disciplina de orientação a objetos. Space Combat Game em Java. Deve-se eliminar as 10 waves de alienígenas para vencer a ameaça!","Informações",JOptionPane.INFORMATION_MESSAGE);
        	}
        });
    	
    	sair.addActionListener(new ActionListener(){
        	
        	public void actionPerformed(ActionEvent e){
        		System.exit(0);
        }});

    	
    }
    
	
	
	
    public static void main(String[] args) {
     
        EventQueue.invokeLater(new Runnable() {
        	     	


			public void run() {

            		Application app = new Application();
            		app.setVisible(true);

            }
            
        });
   }
    
    
    
    
    
    public static void tocaShoot()throws Exception{
    	Sounds.tocaShoot();
    }
    
	public void actionPerformed(ActionEvent arg0) {
	
	}
	
	public static boolean getJogando() {
		return jogando;
	}

	public static void setJogando(boolean jogando) {
		Application.jogando = jogando;
	}
	
	
	
}