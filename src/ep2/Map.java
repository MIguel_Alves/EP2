package ep2;

import java.awt.Color;

import java.awt.Graphics;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;


import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.Timer;


@SuppressWarnings("serial")
public class Map extends JPanel implements ActionListener {

    private final int SPACESHIP_X = 220;
    private final int SPACESHIP_Y = 430;
    private final Timer timer_map;
    private final Image background;
    private final Spaceship spaceship;

	private boolean emJogo;
	private boolean vitoria = false;
    
    private List<Inimigo> inimigos = new ArrayList<Inimigo>();
    private List<Bonus> bonus = new ArrayList<Bonus>(); 
    private List<Life> life = new ArrayList<Life>();
      
    int i,j, l;
    int vidas = 3; 
    public int onda = 0;
	int k= 0;
	int enemieskilled=0;
    
	
    private int score = 0;
    
    public Map() {
        
        addKeyListener(new KeyListerner());
        
        setFocusable(true);
        setDoubleBuffered(true);
        ImageIcon image = new ImageIcon("images/space2.jpg");
        
        this.background = image.getImage();

        spaceship = new Spaceship(SPACESHIP_X, SPACESHIP_Y);

        emJogo = true;
        
        
        
        timer_map = new Timer(Game.getDelay(), this);
        timer_map.start();
                            
    }
    
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        
        g.drawImage(this.background, 0, 0, null);
         
        if(emJogo){        	       	
        	
	        List<Missile> misseis = spaceship.getMisseis();
	        
	        for (int i = 0; i < misseis.size(); i++){
	        	Missile m = (Missile)misseis.get(i); 
	        	g.drawImage(m.getImage(),m.getX(), m.getY(), this);
	        	
	        }
	        
	        for (int i = 0; i < inimigos.size(); i++){
	        	Inimigo in = inimigos.get(i); 
	        	g.drawImage(in.getImage(),in.getX(), in.getY(), this);
	        	
	        }
	        
	        for (int i = 0; i < bonus.size(); i++){
	        	Bonus bo = bonus.get(i); 
	        	g.drawImage(bo.getImage(),bo.getX(), bo.getY(), this);
	        	
	        }
	        
	        for (int i = 0; i < life.size(); i++){
	        	Life li = life.get(i); 
	        	g.drawImage(li.getImage(),li.getX(), li.getY(), this);
	        	
	        }
	        
	        g.setColor(Color.white);
	        g.drawString("SCORE: " + score, 500, 15 );
	        g.drawString("VIDAS: " + vidas, 5,15);
	        
	        if(enemieskilled%20<=02){
	        	
	        	if (onda <=10){
	        		String message = "WAVE: ";
		        	Font font = new Font("Helvetica", Font.BOLD, 14);
		        	FontMetrics metric = getFontMetrics(font);
		        
		        	g.setColor(Color.white);
		        	g.setFont(font);
		        	g.drawString(message + (onda+1), (Game.getWidth() - metric.stringWidth(message)) / 2, Game.getHeight() / 2);
	        	}
			}
	        if (onda>10){
	        	emJogo = false;
	        	vitoria = true;
				
			}
	        
        } 
        else {
        	
        	if(vitoria == true){
        		drawMissionAccomplished(g);
        	}
        	else {
        		drawGameOver(g);
        	}
        }
        
        draw(g);
        

        Toolkit.getDefaultToolkit().sync();
    }

    
    
    
    
    
    private void draw(Graphics g) {
               
        // Draw spaceship
        g.drawImage(spaceship.getImage(), spaceship.getX(), spaceship.getY(), this);
        
        
    }
    
    public void actionPerformed(ActionEvent e) {
       
    	List<Missile> misseis = spaceship.getMisseis();
    	
    	for (int i = 0; i< misseis.size(); i++){
    		Missile m = (Missile)misseis.get(i); 
    		
    		if (m.isVisible()){
    			m.atirar();
    		} else {
    			misseis.remove(i);
    		}
    	}
    	
    	for (int i = 0; i< inimigos.size(); i++){
    		Inimigo in = inimigos.get(i); 
    		
    		if (in.isVisible()){
    			in.atirar();
    		} else {
    			inimigos.remove(i);
    		}
    	}
    	
    	for (int i = 0; i< bonus.size(); i++){
    		Bonus bo = bonus.get(i); 
    		
    		if (bo.isVisible()){
    			bo.moveBonus();
    		} else {
    			bonus.remove(i);
    		}
    	}
    	
    	for (int i = 0; i< life.size(); i++){
    		Life li = life.get(i); 
    		
    		if (li.isVisible()){
    			li.moveLife();
    		} else {
    			life.remove(i);
    		}
    	}
    	
    	
    	if ( emJogo){
    	
    	updateSpaceship();
        checarColisoes();
        repaint();
    	}
    }
    
    @SuppressWarnings("unused")
	private void drawMissionAccomplished(Graphics g) {

        String message = "ALIENÍGENAS DERROTADOS! AMEAÇA CONTIDA! PARABÉNS!";
        Font font = new Font("Helvetica", Font.BOLD, 14);
        FontMetrics metric = getFontMetrics(font);

        g.setColor(Color.white);
        g.setFont(font);
        g.drawString(message, (Game.getWidth() - metric.stringWidth(message)) / 2, Game.getHeight() / 2);
        emJogo =false;
    }
    
 
	private void drawGameOver(Graphics g) {

        String message = "Game Over";
        Font font = new Font("Helvetica", Font.BOLD, 14);
        FontMetrics metric = getFontMetrics(font);
        
        g.setColor(Color.white);
        g.setFont(font);
        g.drawString(message, (Game.getWidth() - metric.stringWidth(message)) / 2, Game.getHeight() / 2);
        spaceship.explode();
        Application.setJogando(false);

    }
    
	
    private void updateSpaceship() {
    	
    	spaceship.move();
    	
    	if(i%8== 0){
    		
    		inimigos.add(new Inimigo(0,0));
    		
    	}
    	i++;
    	if(j%1500 == 0){
    		
    		bonus.add(new Bonus(0,0));
    		
    	}
    	j++;
    	if(l%2000 == 0){
    		
        	life.add(new Life(0,0));
        		
        }
    	l++;
    	
    	
    	
    }
  

    public void checarColisoes(){
    	Rectangle formaNave = spaceship.getBounds();
    	Rectangle formaInimigo;
    	Rectangle formaMissile;
    	Rectangle formaBonus;
    	Rectangle formaLife;
    	
    	
    	
    	for(int i = 0; i < inimigos.size(); i++){
    		Inimigo tempInimigo = inimigos.get(i);
    		formaInimigo = tempInimigo.getBounds();
    		
    		
    		if (formaNave.intersects(formaInimigo)){
    			tempInimigo.explode();
    			tempInimigo.setVisible(false);
    			vidas--;
    			try {
					tocaAiai();
				} catch (Exception e) {
					
					e.printStackTrace();
				}
    			
    			if (vidas<=0){
    				spaceship.explode();
    				spaceship.setVisible(false);
    				emJogo = false;
    				
    			}
    		}
    	}
    	
    	
    	for(int i = 0; i < bonus.size(); i++){
    		Bonus tempBonus = bonus.get(i);
    		formaBonus = tempBonus.getBounds();
    		
    		if (formaNave.intersects(formaBonus)){
    			score+=20;
    			tempBonus.setVisible(false);
    		}
    		
    	}
    	
    	for (int i =0; i<life.size(); i++){
    		Life tempLife = life.get(i);
    		formaLife = tempLife.getBounds();
    		
    		if (formaNave.intersects(formaLife)){
    			vidas++;
    			tempLife.setVisible(false);
    		}
    	}
    	
    	
    	
    	List<Missile> misseis = spaceship.getMisseis();
    	
    	for (int i = 0; i < misseis.size(); i++){
    		Missile tempMissile = misseis.get(i);
    		formaMissile = tempMissile.getBounds();
    		
    		for(int j =0; j < inimigos.size(); j++){
    			Inimigo tempInimigo = inimigos.get(j);
    			formaInimigo = tempInimigo.getBounds();
    			
    			if(formaMissile.intersects(formaInimigo)){
    				
    				try {
						tocaExplosion();
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
    				
    				score++;
    				tempInimigo.explode();
    				tempInimigo.setVisible(false);
    				tempMissile.setVisible(false);
    				k++;
    				enemieskilled++;
    				
    				if(enemieskilled%20==0){
    					onda++;
    					Inimigo.wave = onda;
    				}
    				
    			}
    			
    		}
    	}
    	
    }
    
    
    private class KeyListerner extends KeyAdapter {
        
        @Override
        public void keyPressed(KeyEvent e) {
            spaceship.keyPressed(e);
        }

        @Override
        public void keyReleased(KeyEvent e) {
            spaceship.keyReleased(e);
        }

    }
    

    
    
    public void tocaAiai() throws Exception {
    	Sounds.tocaAiai();
    }
    
    public void tocaExplosion() throws Exception {
    	Sounds.tocaExplosao();
    }
}