package ep2;

import java.io.*;


import sun.audio.*;

/**
 * A simple Java sound file example (i.e., Java code to play a sound file).
 * AudioStream and AudioPlayer code comes from a javaworld.com example.
 * @author alvin alexander, devdaily.com.
 */
public class Sounds {
  public static void tocaAiai() 
  throws Exception
  {
    // open the sound file as a Java input stream
    String gongFile = "sounds/aiai.wav";
    InputStream in = new FileInputStream(gongFile);

    // create an audiostream from the inputstream
    AudioStream audioStream = new AudioStream(in);

    // play the audio clip with the audioplayer class
    AudioPlayer.player.start(audioStream);
  }
  
  public static void tocaShoot() 
	throws Exception {
	  
	    // open the sound file as a Java input stream
	    String gongFile = "sounds/ShootingStars.wav";
	    InputStream in = new FileInputStream(gongFile);

	    // create an audiostream from the inputstream
	    AudioStream audioStream = new AudioStream(in);

	    // play the audio clip with the audioplayer class
	    AudioPlayer.player.start(audioStream);
	    
  }
  
  public static void tocaTiro()
    throws Exception {

	// open the sound file as a Java input stream
	    String gongFile = "sounds/missel2.wav";
	    InputStream in = new FileInputStream(gongFile);

	    // create an audiostream from the inputstream
	    AudioStream audioStream = new AudioStream(in);

	    // play the audio clip with the audioplayer class
	    AudioPlayer.player.start(audioStream);  
	    
	    
  }
  
  public static void tocaExplosao()
		    throws Exception {
			  
			// open the sound file as a Java input stream
			    String gongFile = "sounds/explosion2.wav";
			    InputStream in = new FileInputStream(gongFile);

			    // create an audiostream from the inputstream
			    AudioStream audioStream = new AudioStream(in);

			    // play the audio clip with the audioplayer class
			    AudioPlayer.player.start(audioStream);  
			  
			    
		  }
  
}