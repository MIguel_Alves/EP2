package ep2;

import java.awt.Rectangle;
import java.util.concurrent.ThreadLocalRandom;


public class Inimigo extends Sprite {
	

	private static final int HEIGHT_TELA = 580;

	private static int VELOCIDADE_Y = -4;
	
	public static int wave;
	
	public Inimigo (int x, int y) {
	        super(x, y);
	        this.x = ThreadLocalRandom.current().nextInt(0,560);
	        initInimigo();
	    }

	    private void initInimigo() {
	        
	        fire();
	        
	    }
	    public void fire(){
	    	if (wave <4){
	    		loadImage("images/alien_EASY.png");
	    		
	    	}
	    	if ( 4<= wave && wave <8){
	    		loadImage("images/alien_MEDIUM.png");
	    		VELOCIDADE_Y = -5;
	    	}
	    	if ( wave>=8 && wave<=10){
	    		loadImage("images/alien_HARD.png");
	    		VELOCIDADE_Y = -6;
	    	}
	    }
	    
	    public void atirar(){
	    	
	    	if(this.y < 0){
	    		this.y = HEIGHT_TELA;
	    	} else {
	    		this.y -= VELOCIDADE_Y; 
	    	}
	    }
	    
	    public Rectangle getBounds(){
	    	return new Rectangle(x,y,getWidth(),getHeight());
	    }
	    
	    public void explode(){
	        loadImage("images/explosion.png"); 
	    }
	    	    
}
