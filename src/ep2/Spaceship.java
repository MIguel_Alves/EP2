package ep2;

import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import java.io.*;
import sun.audio.*;


public class Spaceship extends Sprite {
    
    private static final int MAX_SPEED_X = 3; //2;
    private static final int MAX_SPEED_Y = 4; //1;
   
    private int speed_x;
    private int speed_y;
    
    private List<Missile> misseis;

    public Spaceship(int x, int y) {
        super(x, y);
        
        misseis = new ArrayList<Missile>();
        
        initSpaceShip();
    }

    private void initSpaceShip() {
        
        noThrust();
        
    }
    
    private void noThrust(){
        loadImage("images/nave.gif"/*"images/spaceship.png"*/); 
    }
    
    private void thrust(){
        loadImage("images/nave.gif"/* "images/spaceship_thrust.png"*/); 
    }    

    public void move() {
        
        // Limits the movement of the spaceship to the side edges.
        if((speed_x < 0 && x <= 0) || (speed_x > 0 && x + width >= Game.getWidth())){
            speed_x = 0;
        }
        
        // Moves the spaceship on the horizontal axis
        x += speed_x;
        
        // Limits the movement of the spaceship to the vertical edges.
        if((speed_y < 0 && y <= 0) || (speed_y > 0 && y + height >= Game.getHeight())){
            speed_y = 0;
        }

        // Moves the spaceship on the vertical axis
        y += speed_y;
        
    }

    public void keyPressed(KeyEvent tecla) {

        int key = tecla.getKeyCode();
        
        // Set speed to move to the left
        if (key == KeyEvent.VK_LEFT) { 
            speed_x = -1 * MAX_SPEED_X;
        }

        // Set speed to move to the right
        if (key == KeyEvent.VK_RIGHT) {
            speed_x = MAX_SPEED_X;
        }
        
        // Set speed to move to up and set thrust effect
        if (key == KeyEvent.VK_UP) {
            speed_y = -1 * MAX_SPEED_Y;
            thrust();
        }
        
        // Set speed to move to down
        if (key == KeyEvent.VK_DOWN) {
            speed_y = MAX_SPEED_Y;
        }
        
        //
        if(key == KeyEvent.VK_SPACE){
        	atira();
        	
        	
        	try {
				tocaTiro();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        	
        	
        }
        
    }
    
    public void keyReleased(KeyEvent e) {

        int key = e.getKeyCode();

        if (key == KeyEvent.VK_LEFT || key == KeyEvent.VK_RIGHT) {
            speed_x = 0;
        }

        if (key == KeyEvent.VK_UP || key == KeyEvent.VK_DOWN) {
            speed_y = 0;
            noThrust();
        }
    }
    
    public void atira(){
    	this.misseis.add(new Missile(x+getWidth()/3,y+getHeight()/2));
    }

	public List<Missile> getMisseis() {
		return misseis;
	}
	
	public Rectangle getBounds(){
    	return new Rectangle(x,y,getWidth(),getHeight());
    }
	
	public void explode(){
	        loadImage("images/explosion.png"); 
	}
	 
	public void tocaTiro() throws Exception {
	    	Sounds.tocaTiro();
	}

    
}


