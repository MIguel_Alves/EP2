package ep2;

import java.awt.Rectangle;

//import java.awt.event.KeyEvent;

public class Missile extends Sprite {
	
//	@SuppressWarnings("unused")
	private static final int HEIGHT_TELA = 580;//520;
//	@SuppressWarnings("unused")
	private static final int VELOCIDADE_Y = -5;

	public Missile (int x, int y) {
	        super(x, y);
	        
	        initMissile();
	    }

	    private void initMissile() {
	        
	        fire();
	        
	    }
	    private void fire(){
	        loadImage("images/missile.png"); 
	    }
	    
	    public void atirar(){
	    	this.y += VELOCIDADE_Y;
	    	if (this.y > HEIGHT_TELA){
	    		setVisible(false);
	    	}
	    
	    }
	    public Rectangle getBounds(){
	    	return new Rectangle(x,y,getWidth(),getHeight());
	    }
	    
	    public void explode(){
	        loadImage("images/explosion.png"); 
	    }
	    
}
