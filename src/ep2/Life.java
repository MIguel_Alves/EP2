package ep2;

import java.awt.Rectangle;
import java.util.concurrent.ThreadLocalRandom;

public class Life extends Sprite{
	
	private static final int HEIGHT_TELA = 580;

	private static final int VELOCIDADE_Y = -4;
	
	public Life(int x, int y) {
		super(x, y);
		// TODO Auto-generated constructor stub
		 this.x = ThreadLocalRandom.current().nextInt(0,560);
		 initLife();
	}
	
	private void initLife(){
		carregaLife();
	}
	
	private void carregaLife(){
		loadImage("images/life.png");
	}

    public void moveLife(){
    	
    	if(this.y < 0){
    		this.y = HEIGHT_TELA;
    	} else {
    		this.y -= VELOCIDADE_Y; 
    	}
    }
    
    public Rectangle getBounds(){
    	return new Rectangle(x,y,getWidth(),getHeight());
    }

}
