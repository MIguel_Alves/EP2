package ep2;

import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;

public final class Game {
	
	static GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
    private static final int WIDTH = 1150;//gd.getDisplayMode().getWidth();
    private static final int HEIGHT = 1080;//gd.getDisplayMode().getHeight();
    private static final int DELAY = 25;

    
    
    public static int getWidth(){
        return (WIDTH*50/100);
    }
    
    public static int getHeight(){
        return HEIGHT*50/100;
    }
    
    public static int getDelay(){
        return DELAY;
    }

}